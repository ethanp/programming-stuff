'''
Created on Dec 24, 2012
http://stackoverflow.com/questions/100003/
        what-is-a-metaclass-in-python
@author: Ethan
'''
class SimpleClass(object):
    pass

# Create an object of the class itself, I suppose
myObject = SimpleClass()