## MacVim

To do this Action                   | You must take these Steps
------------------------------------|--------------------------
Ack                                 | `ga`
Add execute permissions             | `gh`
Add surrounding to this LINE        | `yss`
Add surrounding to this WORD        | `ysiw`
Insert blanks above / below         | `gio` / `go`
Change surrounding                  | `cs`
Delete surrounding                  | `ds`
Go-To beginning of line (above)     | `gk`
Go-To beginning of line (below)     | `gj`
Go-To beginning of word             | `gw`
Go-To beginning of word (strict)    | `gW`
Indentation guides                  | `gig`
Insert tab-character `\t`           | Ctrl-v + \<`TAB`\>
Reload .vimrc                       | `gvr` (doesnt work)
Open .vimrc in h-split              | `gvs`
Open .vimrc in h-split              | `gvv`
Open .vimrc in new buffer           | `gvw`
Open Command-T                      | `zc`
Open Dropbox in Command-T (nice)    | `zC`
Open TreeBranch Undo                | `gz`
Open Undo Tree                      | `gz`
Re-hardwrap paragraphs of text      | `gq`
Strip trailing whitespace           | `gl`
Switch between open tabs            | Optn + Cmd + ( left / right )
Switch buffers in current window    | F13 / F14 or "print scrn"
Toggle comments                     | *line-count* + `gci`
Print list of `marks` in all files  | `:marks`


# Other nice Vim stuff

#### NERDTree
To do this Action               | You must take these Steps
--------------------------------|--------------------------
Open in horizontally-split tab  | t
Open in vertically-split tab    | s

#### VimAck:
See ack.txt (Sherlock-it)


##### Good SNIPPETS:

**IN a C file:**

prefix      | result
------------|--
`def`       |  define
`do`        |  do/while
`enum`      |  enum declaration
`for`       |  really nice!
`fund`      |  function declaration
`if`        |  if
`inc`       |  include <>
`Inc`       |  include ""
`main`      |  main
`pr`        |  printf
`td`        |  type definition
`tds`       |  struct declaration
`wh`        |  while

**IN a PYTHON file:**

prefix          |  result
----------------|--
`cl`            |  class declaration
`def`           |  function
`deff`          |  simpler version
`for`           |  for
`imp`           |  import
`property`      |  property
`try`           |  buncha options given
`wh`            |  while




`TODO:` The other `Vim` plugins I have (esp. **`Ack`**)

